var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');

app.use(express.static(__dirname + '/public'));

require('./app/minesweeper.js');
require('./app/player.js');
require('./app/game.js');

/* @var games Game[] */
var games = [];

app.get([
    '/beginner*',
    '/intermediate*',
    '/expert*'
], function(req, res) {
    res.sendFile(__dirname + '/public/play.html');
});

function getNumberOfPlayers() {
    var numberOfPlayers = {};
    var i;
    for(i in games) {
        if (typeof games[i] !== "undefined") {
            numberOfPlayers[i] = games[i].players.length;
        }
    }
    return numberOfPlayers;
}

function roomId(data) {
    return data.difficulty + (data.advanced === true ? "_a" : "");
}

io.on('connection', function(socket){
    socket.on('index', function () {
        io.sockets.emit('refresh-players', getNumberOfPlayers());
    });
    
    socket.on('room', function(data){
        socket.join(roomId(data));
        var game;
        if(typeof games[roomId(data)] === 'undefined') {
            game = new Game(data.difficulty, data.advanced, io);
            games[roomId(data)] = game;
            gameData = game.create();
        } else {
            game = games[roomId(data)];
            gameData = game.getData();
        }
        game.addPlayer(new Player(data.name, socket.id));
        
        io.to(roomId(data)).emit('get data', gameData);
        io.to(roomId(data)).emit('message-receive', ['system', 'new player joined']);
        io.sockets.emit('refresh-players', getNumberOfPlayers());
    });
    
    socket.on("send data", function(data){
        var to = io.to(roomId(data));
        var game = games[roomId(data)];
        res = game.do(data.data);
        var player = game.getPlayer(socket.id);
        io.to(roomId(data)).emit("update data", res);
        if (res.status === "lost") {
            to.emit("message-receive", ['system', 'Dumbass <u>' + player.name  + '</u> stepped on a bomb. Congratulations...']);
        }
        if (res.status === "won") {
            to.emit("message-receive", ['system', 'Well done! you won! Hurray...']);
        }
    });
    
    socket.on("message-send", function(data){
        player = games[roomId(data)].getPlayer(socket.id);
        io.to(roomId(data)).emit("message-receive", [player.name, data.message]);
    });

    socket.on("reset", function(data){
        io.emit('get data', games[roomId(data)].reset());
    });

    socket.on('disconnect', function () {
        for(var i in games) {
            if (typeof games[i] === "undefined") {
                continue;
            }
            var game = games[i];
            player = game.getPlayer(socket.id);
            if (player) {
                game.removePlayer(socket.id);
                io.to(roomId({difficulty:game.getDifficulty(), advanced: game.isAdvanced()})).emit('message-receive', ['system', 'Player disconnected']);
                io.sockets.emit('refresh-players', getNumberOfPlayers());
                if (game.players.length === 0) {
                    games[i] = undefined;
                }
            }
        };
    })
});

http.listen(3000, '0.0.0.0',  function(){
    console.log('listening on *:3000');
});


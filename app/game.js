global.Game = function (difficulty, advanced, io) {

    var advancedTimeouts = {
        beginner: 20,
        intermediate: 135,
        expert: 330
    };
    var time = advanced ? advancedTimeouts[difficulty] : 0;

    var timer = false;

    this.players = [];

    var minesweeper = new Minesweeper(difficulty);
    
    var lastAffectedFields = [];

    this.create = function () {
        minesweeper.create();
        return this.getData();
    };

    function startTimer () {
        clearTimer(timer);
        timer = setInterval(doTimer, 1000);
    }

    function doTimer() {
        advanced ? time-- : time++;
        if (advanced && time <= 0) {
            minesweeper.setStatus('lost');
            io.to(difficulty).emit('get data', getDataReal());
        }
    }

    function stopTimer() {
        clearInterval(timer);
    }

    function clearTimer() {
        time = advanced ? advancedTimeouts[difficulty] : 0;
    }

    this.reset = function () {
        minesweeper.reset();
        stopTimer();
        clearTimer();
        return this.getData();
    };


    this.getData = function (clickedField) {
        return getDataReal(clickedField);
    };
    
    function getDataReal(clickedField) {
        if (minesweeper.getStatus() !== "playing") {
            stopTimer();
        }
        return {
            fields: getFieldsData(clickedField),
            status: minesweeper.getStatus(),
            time: time,
            bombsLeft: minesweeper.getBombsLeft(),
            advanced: advanced
        };
    }

    function getFieldsData(clickedField) {
        var fieldsData = [];
        var fields;
        if (lastAffectedFields.length > 0 ) {
            fields = lastAffectedFields;
        } else {
            fields = minesweeper.getFields();
        }
        for (var i in fields) {
            var field = fields[i];
            fieldsData.push({
                X: field.X,
                Y: field.Y,
                status: getFieldStatus(field)
            });
            
            if (typeof clickedField !== "undefined" && clickedField.X === field.X && clickedField.Y === field.Y && field.hasMine) {
                fieldsData[i].status += " clicked";
            }
        }
        lastAffectedFields = [];
        return fieldsData;
    }

    function getFieldStatus(field)
    {
        if (!field.isOpened) {
            if (!advanced && field.hasFlag) {
                return minesweeper.getStatus() === 'lost' && !field.hasMine ? 'fa fa-flag mistake' : 'fa fa-flag';
            }
            return 'hidden';
        }
        if (field.hasMine) {
            return 'fa fa-bomb';
        }

        return 'revealed:' + field.neighbourMineCount;

    }

    this.do = function(clickedField, player) {

        if (minesweeper.getStatus() === "ready") {
            startTimer();
        }
        
        if (!advanced || clickedField.action !== 'toggleFlag') {
            lastAffectedFields = minesweeper.do(clickedField);
        }
        return this.getData(clickedField);
    };

    /**
     *
     * @param id
     * @returns Player
     */
    this.getPlayer = function(id) {
        var index;
        for (index in this.players) {
            if (this.players[index].id === id) {
                return this.players[index];
            }
        }

        return null;
    };


    this.getNumberOfPlayers = function () {
        return this.players.length;
    };

    /**
     * @param player
     */
    this.addPlayer = function(player)
    {
        this.players.push(player);
    };

    /**
     * @param id int
     */
    this.removePlayer = function(id) {
        this.players.splice(this.getUserIndexById(id));
    };

    /**
     * @param id int
     * @returns int|null
     */
    this.getUserIndexById = function(id)
    {
        var index;
        for (index in this.players) {
            if (this.players[index].id === id) {
                return index;
            }
        }
        return null;
    };
    
    this.isLost = function () {
        return minesweeper.getStatus() === 'lost';
    };
    
    this.getDifficulty = function () {
        return difficulty;
    };

    this.isAdvanced = function () {
        return advanced === true;
    }
};
global.Minesweeper = function(difficulty) {

    var settings = {
        beginner: {
            rows: 9,
            cols: 9,
            bombs: 10
        },
        intermediate: {
            rows: 16,
            cols: 16,
            bombs: 40
        },
        expert: {
            rows: 16,
            cols: 30,
            bombs: 99
        }
    };

    var fields = [];

    var attribs = settings[difficulty];

    var neighbourIndexes = [
        [-1, -1],
        [-1, 0],
        [-1, 1],
        [0, -1],
        [0, 1],
        [1, -1],
        [1, 0],
        [1, 1]
    ];

    var hiddenFields;

    var status = 'ready';
    
    var lastAffectedFields = [];

    function doCreate()
    {
        hiddenFields = attribs.rows * attribs.cols;
        var arr = [];
        for(i = 1; i <= attribs.bombs; i++){
            arr.push({
                hasMine: 1,
                isOpened: false,
                hasFlag: false,
                neighbourMineCount: 0
            });
        }
        for(j = 1; j <= (attribs.rows * attribs.cols) - attribs.bombs; j++){
            arr.push({
                hasMine: 0,
                isOpened: false,
                hasFlag: false,
                neighbourMineCount: 0
            });
        }
        arr = shuffle(arr);
        var i,j,temparray,chunk = attribs.cols;
        for (i=0,j=arr.length; i<j; i+=chunk) {
            temparray = arr.slice(i,i+chunk);
            fields.push(temparray);
        }
        countNeighbourMines();
    };
    
    this.create = function () {
        doCreate();
    };

    this.getFields = function () {
        var toReturn = [];
        for (var i in fields) {
            for (var j in fields[i]) {
                var field = fields[i][j];
                toReturn.push({
                    hasMine: field.hasMine,
                    hasFlag: field.hasFlag,
                    isOpened: field.isOpened,
                    neighbourMineCount: field.neighbourMineCount,
                    X: j,
                    Y: i
                });
            }
        }
        return toReturn;
    };

    function shuffle(a) {
        var j, x, i;
        for (i = a.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
        return a;
    }

    function countNeighbourMines()
    {
        for(i = 0; i < fields.length; i++){
            var row = fields[i];
            for(j = 0; j < row.length; j++){
                var col = row[j];
                neighbourIndexes.forEach(function(val, index){
                    X = j + val[1];
                    Y = i + val[0];
                    neighbour = getField(Y, X);
                    if(neighbour && neighbour.hasMine){
                        col.neighbourMineCount++;
                    }
                });
                updateField(i, j, col);
            }
        }
    }

    function getField(row, col)
    {
        if(fieldExists(row, col)){
            return fields[row][col];
        }

        return null;
    }

    function fieldExists(row, col) {
        if(typeof fields[row] !== 'undefined'){
            if(typeof fields[row][col] !== 'undefined'){
                return true;
            }
        }
        return false;
    }

    function updateField(row, col, val)
    {
        if(fieldExists(row, col)){
            fields[row][col] = val;
        }
    }

    function checkField(Y, X){
        var field = getField(Y, X);
        if(!field.hasFlag){
            if(!field.isOpened){
                updateField(Y, X, field);
                if (!field.hasMine) {
                    field.isOpened = true;
                    checkNeighbourFields(Y, X, field);
                    hiddenFields--;
                } else {
                    field.isOpened = true;
                    explode();
                }
            }
        }

        if (hiddenFields - attribs.bombs === 0) {
            win();
        }
        lastAffectedFields.push({
            hasMine: field.hasMine,
            hasFlag: field.hasFlag,
            isOpened: field.isOpened,
            neighbourMineCount: field.neighbourMineCount,
            X: X,
            Y: Y
        });
    }
    

    function checkNeighbourFields(Y, X, field)
    {
        neighbourIndexes.forEach(function(val, i){
            nX = X + val[1];
            nY = Y + val[0];
            neighbour = getField(nY, nX);
            if(neighbour){
                if(((!neighbour.hasMine && !neighbour.hasFlag && neighbour.neighbourMineCount === 0)
                || field.neighbourMineCount === 0)
                && !neighbour.isOpened
                ){
                    checkField(nY, nX);
                }
            }
        });
    }

    this.getBombsLeft = function () {
        var left = attribs.bombs;
        for (i in fields) {
            for (j in fields[i]) {
                if (fields[i][j].hasFlag) {
                    left--;
                }
            }
        }
        return left;
    };

    function hasRevealed() {
        for (i in fields) {
            for (j in fields[i]) {
                if (fields[i][j].isOpened) {
                    return true;
                }
            }
        }
        return false;
    }
;
    function win() {
        status = 'won';
        for (i in fields) {
            for (j in fields[i]) {
                if (fields[i][j].hasMine && !fields[i][j].hasFlag) {
                    fields[i][j].hasFlag = true;
                }
            }
        }
    }

    function explode()
    {
        status = 'lost';
        for(i = 0; i < fields.length; i++){
            var row = fields[i];
            for(j = 0; j < row.length; j++){
                var field = row[j];
                if (!field.isOpened && !field.hasFlag && field.hasMine) {
                    field.isOpened = true;
                    updateField(i, j, field);
                    lastAffectedFields.push({
                        hasMine: field.hasMine,
                        hasFlag: field.hasFlag,
                        isOpened: field.isOpened,
                        neighbourMineCount: field.neighbourMineCount,
                        X: j,
                        Y: i
                    });
                }
                
            }
        }
    }

    function toggleFlag(Y, X)
    {
        var field;
        field = getField(Y, X);
        if(!field.isOpened){

            if(!field.hasFlag){
                field.hasFlag = true;
            } else{
                field.hasFlag = false;
            }
            updateField(Y, X, field);
        }
        
        lastAffectedFields.push({
            hasMine: field.hasMine,
            hasFlag: field.hasFlag,
            isOpened: field.isOpened,
            neighbourMineCount: field.neighbourMineCount,
            X: X,
            Y: Y
        });
    }

    this.do = function(data) {
        return realDo(data);
    };
    
    function realDo(data) {
        status = "playing";
        action = data.action;
        switch (action){
            case 'checkField':
                checkField(data.Y, data.X);
            break;

            case 'toggleFlag':
                toggleFlag(data.Y, data.X);
            break;
        }
        var modifiedFields = lastAffectedFields;
        lastAffectedFields = [];
        return modifiedFields;
    }

    this.reset = function(){
        realReset();
    };
    
    function realReset() {
        status = 'ready';
        fields = [];
        doCreate();
    }

    this.getStatus = function () {
        return status;
    };
    
    this.setStatus = function (newStatus) {
        status = newStatus;
    };

};

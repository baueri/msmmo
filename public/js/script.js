const difficulties = {
    beginner: 'beginner',
    intermediate: 'intermediate',
    expert: 'expert'
};

var urlParts = window.location.pathname.split('/');

var difficulty = difficulties[urlParts[1]];
var advanced = urlParts[2] === "advanced";
var socket = io('/', {difficulty:difficulty});
var name = "";
while (name === "") {
    name = prompt("Mi a neved, zsidó?");
}

socket.emit('room', {difficulty: difficulty, name: name, advanced: advanced});
var status;
var time = 0;
var timer = false;
var data = {};
jQuery(document).ready(function(){
    var table = jQuery("#board");
    var smile = $("#smile");

    $(this).on("mouseup", function () {
        if (status === "playing" || status === "ready") {
            smile.attr("class", "");
        }
    });
    table.on("mousedown", "td", function (e) {
        if (e.which === 1 && $(".hidden", $(this)).length > 0) {
            smile.addClass("curious");
        }
    }).on("mouseup", "td", function(e) {
        if (status === 'won' || status === 'lost') {
            return;
        }
        clicked = e.which;
        var Y = jQuery(this).parent().index();
        var X = jQuery("td", jQuery(this).parent()).index(jQuery(this));
        data = {
            Y: Y,
            X: X
        };
        switch(clicked){
            case 1:
                data.action = 'checkField';
            break;

            case 3:
                e.preventDefault();
                data.action = 'toggleFlag';
            break;
        }

        socket.emit("send data", realData(data));
    });

    socket.on("get data", function(receivedData){
        status = receivedData.status;
        data = receivedData;
        table.html("");
        updateGameStatus(receivedData);
        var fields = receivedData.fields;
        var rowDOM = jQuery("<tr></tr>");
        for (var i in fields) {
            var field = fields[i];
            no = tdC = "";
            if(field.status.startsWith('revealed')){
                no = field.status.split(':')[1];
                tdC = 'class="revealed"';
            } else {
                tdC = 'class="' + field.status + '"';
            }
            var colDOM = $("<td></td>");
            colDOM.append("<div data-val='" + (no > 0 ? no : "") + "' "+ tdC + "></div>");
            if (typeof fields[i-1] !== "undefined" && fields[i-1].Y !== field.Y) {
                table.append(rowDOM);
                rowDOM = jQuery("<tr></tr>");
            }
            rowDOM.append(colDOM);
        }
        table.append(rowDOM);
    });

    socket.on("update data", function(receivedData){
        status = receivedData.status;
        data = receivedData;
        updateGameStatus(receivedData);
        if(typeof receivedData.fields !== 'undefined'){
            var fields = receivedData.fields;
            for (var i in fields) {
                var fieldData = fields[i];
                field = jQuery("tr:nth-child("+(fieldData.Y+1)+") td:nth-child("+(fieldData.X+1)+")");
                statusParsed = fieldData.status.split(':');
                stat = statusParsed[0];

                field.children("div").attr("class", stat);
                if (stat === "revealed") {
                    var no = statusParsed[1];
                    field.children("div").attr('data-val', no > 0 ? no : '');
                }
            }
        }

    });


    smile.on("click", function(){
        resetTimer();
        socket.emit("reset", {difficulty:difficulty, advanced: advanced});
        smile.attr("class", "");
        table.removeClass('inactive');
    });
    var log = jQuery("#log");
    var messageHider = false;
    log.on("click", function(){
        log.fadeOut('fast');
    });
    socket.on("message-receive", function(data){
        clearTimeout(messageHider);

        if (jQuery("div", log).length === 10) {
            jQuery("div", log).first().remove();
        }
        log.append("<div><span class='user"+(data[0] === 'system' ? ' admin' : '')+"'>[" + data[0] + "]:</span> " + data[1] + "</div>")
            .stop()
            .fadeIn('fast');
        messageHider = setTimeout(function() {
            log.stop().fadeOut('slow');
        }, 4000);
        
    });
    
    $("#chat").on("submit", function(e){
        e.preventDefault();
        var message = $("input", $(this)).val().trim();
        if (!message) {
            return;
        }
        $("input", $(this)).val("");
        socket.emit("message-send", {message: message, difficulty: difficulty, advanced: advanced});
    });

    $("body").on("keydown", function (e) {
        if (!$("#msg").is(":focus") ) {
            $("#msg").focus();
        }
    });

    function updateGameStatus(data) {
        advanced = data.advanced;
        $("#bombsleft span").text(data.bombsLeft);
        time = data.time;
        if (data.status === "playing" && !timer) {
            startTimer();
        } else {
            if (data.status === 'won' || data.status === 'lost') {
                table.addClass('inactive');
                smile.attr("class", status);
                stopTimer();
            } else {
                table.removeClass('inactive');
                smile.attr("class", "");
            }
            updateTimer();
        }
    }
    
    function startTimer() {
        timer = setInterval(doTimer, 1000);
    }

    function doTimer()
    {
        updateTimer(advanced ? --time : ++time);
    }

    function updateTimer() {
        $("#timer span").text(time);
    }

    function resetTimer() {
        stopTimer();
        time = advanced ? data.time : 0;
        updateTimer();
    }

    function stopTimer() {
        clearInterval(timer);
        timer = false;
    }


});

function realData(data){
    return {data: data, difficulty: difficulty, advanced: advanced};
}

